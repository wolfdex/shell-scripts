#!/usr/bin/env bash
# W.-Marcel Richter (wmrichter@gmail.com)

# extract (copy) audio from video without reencoding
#
# using: ffmpeg, ffprobe, sed
#

extract_audio () {
	FILE="${1}"
	
        if [ ! -f "${FILE}" ]; then
	   return 0
	fi

	acodec=$(ffprobe  -show_format -print_format csv "${FILE%}" 2>&1 |   grep 'Audio:' | sed 's/^.*Audio://;s/\,.*//;s/^[[:blank:]]*//;s/[[:blank:]]*$//')

	acodec=$(sed  's/^.*Audiocodec://;s/[[:blank:]].*//;s/^[[:blank:]]*//;s/[[:blank:]]*$//' <<< $acodec)
	echo "Audiocodec: $acodec"

	if [ "$acodec" = "aac" ]; then
	   acodec="m4a"
	fi
	
	ffmpeg -i "${FILE}"  -vn -acodec copy "${FILE%}.$acodec";
        
        return 1
}


if [ -n "$1" ]; then
    extract_audio "${1}"
else
    for FILE in *.webm *.flv *.mp4 *.ts ; do
	extract_audio "${FILE}"
    done;
fi


