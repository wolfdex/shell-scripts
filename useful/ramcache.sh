#!/bin/bash
# W.-M. Richter

case "$1" in

  --save) rsync -a /tmp/ucache/$(whoami)/ ~/.ramdisk
   ;;
   
  --load) rsync -ah ~/.ramdisk/ /tmp/ucache/$(whoami) --stats >> ~/log/ramdisk.log
   
   ;;
   
  --free-cache) 
      rm -rvf ~/.cache/*  >> ~/log/ramdisk.log
      rm -rvf ~/.cache/.*  >> ~/log/ramdisk.log
      ;;
      
  --free)

      rm -rvf ~/ramdisk/* >> ~/log/ramdisk.log
      rm -rvf ~/ramdisk/.* >> ~/log/ramdisk.log
   
   ;;

   
   *)
      mkdir -vp ~/log/
      date > ~/log/ramdisk.log
      echo  "ramcache.sh started"  >> ~/log/ramdisk.log
      rm -rvf ~/.cache  >> ~/log/ramdisk.log
      rm -rvf ~/ramdisk >> ~/log/ramdisk.log
      mkdir -vp /tmp/ucache/$(whoami)/.cache >> ~/log/ramdisk.log
      ln -svf /tmp/ucache/$(whoami)/.cache  ~/.cache >> ~/log/ramdisk.log
      ln -svf /tmp/ucache/$(whoami)  ~/ramdisk >> ~/log/ramdisk.log
      ;;
esac





