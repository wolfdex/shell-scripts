#!/bin/bash
# W.-M. Richter

## defaults

CON_OPT_FT="pdf"
#CON_OPT_CQOUAL="100"
CON_OPT_CO_DEFAULT="JPEG" # None, BZip, Fax, Group4, JPEG, JPEG2000, Lossless, -LZW-, RLE or Zip. -quality compress  -list compress
#CON_OPT_RSIZE="1240x" # 1240x1754
#CON_OPT_UNITS="PixelsPerInch"
#CON_OPT_DENS="150"
#CON_OPT_REPAGE="+repage"
#CON_OPT_PAGE="a4"


if [[ $# -eq 0 ]]; then
 echo "nothing to do"
 exit 1
fi


if [[ $1 == "-h" ]]; then
  echo "con2x.sh"
  echo "" 
  echo "con2x.sh Image.png Image.png ..."
  echo "con2x.sh -t pdf -s Image.png Image.png ..."
  echo "-o Targetfile.pdf" 
  echo "-t filetype {pdf (default), jpg, png, ... }"
  echo "-tp targetpath"
  echo
  echo "-cd cut digits"
  echo "" 
  echo "-sp sourcefile pack "
  echo "-sr sourcefile remove "
  echo "-srp sourcefile pack and remove "  
  echo ""  
  echo "profiles: "
  echo ""   
  echo "a4       () PDF (jpeg)"
  echo "a4d300	  best Practice        "
  echo "a4d150        "
  echo "a4d150r       "
  echo "a4d72         "  
  echo "a4s          "  
  echo "a4d300G4"
  echo "a4d150G4"
  echo ""
  echo "con2x.sh *.png -profiles a4"
  echo ""
  echo "override"
  echo "con2x.sh *.png -profiles a4 -ct Zip  "
 exit 0
fi


if [[ $# -eq 1 ]]; then
   SrcFile=$1 
else  
  while [[ "$#" -gt "0" ]]; do
   case $1 in
      --DEBUG) OPT_DEBUG=1 # More Messages for DEBUG 
          shift ;
          ;;   
           
      -t) # Filetype / fileextension
          CON_OPT_FT=$2 
          
          shift 2;
          ;;
          
      -ct) # compresstype - JBIG2, JPEG,  Fax, ...
          CON_OPT_CO=$2

          shift 2;
          ;;  

      -o) # output / Targetfile
          tf=$2  
          shift 2;
          ;;
          
      -sr) # remove sourcefile(s)
          sr=1
          shift ;
          ;;          
          
      -sp) # pack sourcefile(s) 
          sp=1
          shift ;
          ;;                             
          
      -spr) # pack & remove sourcefile(s) 
           sr=1
           sp=1
           shift ;
           ;;       
          
      -cd) # cut digits
          cd='r'; 
          shift ;
          ;;               
          
       -cld) # cut leading digits, not impemented
          cd='l'; 
          shift ;
          ;;      
          

       -profile) # default - profiles
          # defaults, dont override given args
            [ -z $CON_OPT_CO ]  &&  CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="98"
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="1240x" # 1240x1754
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 

          case $2 in
          a4) 
           # [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL=""
           # [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="" # 1240x1754
            [  -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 

            ;;   
            
           a4d300)  
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="98"
            [ -z $CON_OPT_RSIZE ] &&  CON_OPT_RSIZE="2480x" # 2480x3508
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="300"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 
           
            ;;   
            
            a4d300G4)  
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="98"
            [ -z $CON_OPT_RSIZE ] &&  CON_OPT_RSIZE="2480x" # 2480x3508
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="300"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 
           
            ;;   
            
            a4d150)  
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="89"
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="1240x" # 1240x1754
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 
            ;;              

            
            a4d150G4)  
            [ -z $CON_OPT_CO ] && CON_OPT_CO="Group4"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="89"
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="1240x" # 1240x1754
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 
            ;;                      
            
                        
          a4d150r)  
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="89"  # defaul ~90
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="1240x" # 1240x1754
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_REPAGE ] && CON_OPT_REPAGE="+repage"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4" 
             
                      
            ;;              
                         
           a4s)  
           
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="89"  # defaul ~90
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="960x"             # old 826x1169   
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_REPAGE ] && CON_OPT_REPAGE="+repage"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4"                 
             
            ;;                      
            
           a4d72)  
           
            [ -z $CON_OPT_CO ] && CON_OPT_CO="jpeg"
            [ -z $CON_OPT_FT ] && CON_OPT_FT="pdf"
            [ -z $CON_OPT_COQUAL ] && CON_OPT_COQUAL="89"  # defaul ~90
            [ -z $CON_OPT_RSIZE ] && CON_OPT_RSIZE="595x" # 595x842
            [ -z $CON_OPT_UNITS ] && CON_OPT_UNITS="PixelsPerInch"
           # [ -z $CON_OPT_DENS ] && CON_OPT_DENS="150"
            [ -z $CON_OPT_REPAGE ] && CON_OPT_REPAGE="+repage"
            [ -z $CON_OPT_PAGE ] && CON_OPT_PAGE="a4"                    
           
            ;;                        
            
          esac
          
          shift 2;
          ;;   
          
       #-cc)  # convert options, not implemented
         #  shift
         #   while [[ $# -gt 0 && -n ${1%%-*}   ]]     
	 #     do 
	 #	CON_OPT="$CON_OPT $2"
	 #	
	 #	echo $tp
	 #	shift
	 #     done
         #
	 #   if [[ ! -z ${tp%%*/}  ]]; then    
	 #    tp="$tp/"
	 #   fi
         # ;;    
          
      -tp) # targetpath 
	    shift
            while [[ $# -gt 0 && -n ${1%%-*}   ]]     
	      do 

		tp="$tp $1"
		
		echo $tp
		shift
	      done

	     if [[ ! -z ${tp%%*/}  ]]; then    
	       tp="$tp/"
	     fi

          ;;           
          
      -s) # sourcefiles -> *
          shift;;
       *) 
	    if [[ ! -n ${1%%-*} ]]; then
                echo "surprising argument $1"
		exit 1
	    fi
	    
	    # get sourcefiles 
            while [[ $# -gt 0 && -n ${1%%-*} ]]     
	      do     
		SrcFile="$SrcFile $1"
		shift                
	      done

	      SrcFile="${SrcFile#"${SrcFile%%[![:space:]]*}"}"   # remove leading whitespace characters
              SrcFile="${SrcFile%"${SrcFile##*[![:space:]]}"}"   # remove trailing whitespace characters                     
          ;;
   esac
done

fi

###----- defaults

[ -z $CON_OPT_CO ] && CON_OPT_CO=$CON_OPT_CO_DEFAULT

###-------------


# sourcefile

if [[ -z $SrcFile ]]; then
  echo "nothing to do - $SrcFile"
  exit 0
fi

# no Targetfile
if [[ -z $tf ]]; then

 tf=$SrcFile

 # tf=$( basename $tf ".${tf##*.}" )  # get basename

  tf=${tf%%.${tf##*.}}
  tf=${tf##*\ } 
  
fi

  # cut digits
if [  ! -z $cd ]; then 
   #tf=$( basename $tf "${tf##*[:digit:]}" ) 
      _hlp=${tf##*[:digit:]}
 
   [ ! -z $OPT_DEBUG ] && echo $_hlp
   tf=${tf%%$_hlp}

fi

# fileextension

tf=${tf}.${CON_OPT_FT}
tfp=$tp$tf # Targetfile with targetpath

# ---------

if [[  "$CON_OPT_CO" == "Fax"  ||  "$CON_OPT_CO" == "Group4"   ]]; then 

  # [ ! -z $CON_OPT_CO ] && CON_OPT=" $CON_OPT -compress $CON_OPT_CO"
  CON_OPT_CO=" $CON_OPT_CO -type bilevel"    

fi
  
   
if [[ $CON_OPT_FT == "ps" ]]; then
  
  if [ ! "$CON_OPT_CO" == "" ] ; then
     CON_OPT=" $CON_OPT -compress $CON_OPT_CO"
  fi   

  [ ! -z $CON_OPT_CQOUAL ] && CON_OPT=" $CON_OPT -quality $CON_OPT_CQOUAL"
  [ ! -z $CON_OPT_RSIZE ] && CON_OPT=" $CON_OPT -resize $CON_OPT_RSIZE"
  [ ! -z $CON_OPT_UNITS ] && CON_OPT=" $CON_OPT -units $CON_OPT_UNITS"
  [ ! -z $CON_OPT_DENS ] && CON_OPT=" $CON_OPT -density $CON_OPT_DENS"
  [ ! -z $CON_OPT_PAGE ] && CON_OPT=" $CON_OPT -page $CON_OPT_PAGE"
  [ ! -z $CON_OPT_REPAGE ] && CON_OPT=" $CON_OPT $CON_OPT_REPAGE"           
        
  [ ! -z $CON_OPT_PAGE ] && GS_OPT=" $GS_OPT -sPAPERSIZE=$CON_OPT_PAGE"
  
  convert $SrcFile $CON_OPT  ps:- > $tfp           
     
elif [[ $CON_OPT_FT == "pdf" ]]; then 


  if [ ! "$CON_OPT_CO" == "" ] ; then
     CON_OPT=" $CON_OPT -compress $CON_OPT_CO"
  fi   
  
  [ ! -z $CON_OPT_CQOUAL ] && CON_OPT=" $CON_OPT -quality $CON_OPT_CQOUAL"
  [ ! -z $CON_OPT_RSIZE ] && CON_OPT=" $CON_OPT -resize $CON_OPT_RSIZE"
  [ ! -z $CON_OPT_UNITS ] && CON_OPT=" $CON_OPT -units $CON_OPT_UNITS"
  [ ! -z $CON_OPT_DENS ] && CON_OPT=" $CON_OPT -density $CON_OPT_DENS"
  [ ! -z $CON_OPT_PAGE ] && CON_OPT=" $CON_OPT -page $CON_OPT_PAGE"
  [ ! -z $CON_OPT_REPAGE ] && CON_OPT=" $CON_OPT $CON_OPT_REPAGE"           
          
  [ ! -z $CON_OPT_PAGE ] && GS_OPT=" $GS_OPT -sPAPERSIZE=$CON_OPT_PAGE"
       
   # convert $SrcFile $CON_OPT  ps:- | ps2pdf  $GS_OPT - $tfp 
   # convert $SrcFile $CON_OPT  $tfp 
   
   convert $SrcFile  $CON_OPT  pdf:- > $tfp 
     
elif [[ $CON_OPT_FT == "png" ]]; then 

  #[ ! -z $CON_OPT_CO ] && CON_OPT=" $CON_OPT -compress $CON_OPT_CO"
  #[ ! -z $CON_OPT_CQOUAL ] && CON_OPT=" $CON_OPT -quality $CON_OPT_CQOUAL"
  [ ! -z $CON_OPT_RSIZE ] && CON_OPT=" $CON_OPT -resize $CON_OPT_RSIZE"
  [ ! -z $CON_OPT_UNITS ] && CON_OPT=" $CON_OPT -units $CON_OPT_UNITS"
  #[ ! -z $CON_OPT_DENS ] && CON_OPT=" $CON_OPT -density $CON_OPT_DENS"
  [ ! -z $CON_OPT_PAGE ] && CON_OPT=" $CON_OPT -page $CON_OPT_PAGE"
  [ ! -z $CON_OPT_REPAGE ] && CON_OPT=" $CON_OPT $CON_OPT_REPAGE"           
          
  [ ! -z $CON_OPT_PAGE ] && GS_OPT=" $GS_OPT -sPAPERSIZE=$CON_OPT_PAGE"  
  
  convert $SrcFile $CON_OPT  png:- > $tfp.png

elif [[ $CON_OPT_CO == "Fax" || $CON_OPT_CO == "Group4"  ]]; then 
   
  CON_OPT_FT="tif"
  #tf=${tf}.${CON_OPT_FT}
#
  
  #[ ! -z $CON_OPT_CQOUAL ] && CON_OPT=" $CON_OPT -quality $CON_OPT_CQOUAL"
  #[ ! -z $CON_OPT_RSIZE ] && CON_OPT=" $CON_OPT -resize $CON_OPT_RSIZE"
  #[ ! -z $CON_OPT_UNITS ] && CON_OPT=" $CON_OPT -units $CON_OPT_UNITS"
  #[ ! -z $CON_OPT_DENS ] && CON_OPT=" $CON_OPT -density $CON_OPT_DENS"
  #[ ! -z $CON_OPT_PAGE ] && CON_OPT=" $CON_OPT -page $CON_OPT_PAGE"
  #[ ! -z $CON_OPT_REPAGE ] && CON_OPT=" $CON_OPT $CON_OPT_REPAGE"           
          
  # [ ! -z $CON_OPT_PAGE ] && GS_OPT=" $GS_OPT -sPAPERSIZE=$CON_OPT_PAGE" 
  # convert input.tif -compress Group4 -adaptive-resize 75% -density 200 -type bilevel TIFF:- | convert - output.pdf
 
  tfp=${tfp%%.${tfp##*.}}
  tfp="$tfp.tif"

  convert $SrcFile $CON_OPT  TIFF:- > $tfp
   
elsefl	
   CON_OPT="-compress $CON_OPT_CO_DEFAULT"   

   convert $SrcFile $CON_OPT $tfp           
fi              

echo $tfp

# pack sourcefiles
if [[ $? -eq 0 && $sp -eq 1 ]]; then

  tar cj $SrcFile  -f  $tfp.origin.tar.bz2
fi

# remove sourcefiles
if [[ $? -eq 0 && $sr -eq 1 ]]; then
  rm  $SrcFile
fi
  
exit 0
  
               
